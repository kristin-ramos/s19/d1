console.log(`hello`)

/* 

Exponent Operator

	const SQUARED = Math.pow(4,3);
	console.log(SQUARED);

	const SQ = 4 ** 3;
	console.log(SQ);

Template Literals
	-use Backticks and dollar $ + curly brace {}`)

Array Destructuring
	
	syntax:
	let [var, var, var]=refArray

	let name=["Cardo","Dela Cruz","Dalisay"]
	// console.log(name[0]);
	// console.log(name[1]);
	// console.log(name[2]);

	let [fName, mName, lName] = name;
	console.log(fName);
	console.log(mName);
	console.log(lName);

	const grades = [91.5, 82.8, 90.9, 85.2]

	function herGrades(arrOfGrades){
		// console.log(arrOfGrades)
		const[one, two, , four]=arrOfGrades
		// console.log(one);
		// console.log(two);
		// console.log(four);

		return `Her grade in English is ${one}, in Math is ${two}, in Science ${four}`

	}

	console.log(herGrades(grades))


OBJECT DESTRUCTURING
	
	syntax:
	let {pro, pro, pro}=refObject

	let flower={
		floName:"Rose",
		type: "thornless",
		color: "red"
	}

	// let {floName, type, color, season} = flower;

	// console.log(floName);
	// console.log(type);
	// console.log(color);
	// console.log(season);


	function describeFlower(bulaklak){
		console.log(bulaklak)
		let {floName, type, color}=bulaklak
		// console.log(floName);
		// console.log(type);
		// console.log(color);

		return `I have a ${color} ${type} ${floName}`
	}

	console.log(describeFlower(flower))

ARROW FUNCTION

	syntax:
*/


const introduce = (name) => {

	return `My name is ${name}`
} 
 
console.log(introduce("Matthew"));
console.log(introduce("Kris"));


/* Explicit*/
const grades = [91.5, 82.8, 90.9, 85.2]

grades.forEach((grade)=>{
	console.log(grade)
})

let mapGrades = grades.map(grade=>{
	return grade
})

console.log(mapGrades)




let name=["Cardo","Dela Cruz","Dalisay"]


/*Implicit*/
name.forEach(person=> console.log(person))

let mappedName = name.map(person => person)
console.log(mappedName)


// default unction argument value

const greet = (name) => `Good morning ${name}`;
console.log(greet());
console.log(greet("Marc"));

function greeting(name = "user"){
		return `Good am ${name}`
	}

console.log(greet());
console.log(greet("Matthew"));


/* CLASS BASED OBJECT */

class className{
	constructor(propertyA, propertyB){
		this.objProperty = propertyA;
		this.objProperty = propertyB
	}
}


class Car{
	constructor(name, model, make, color, type){
		this.carName=name;
		this.model=model;
		this.make=make;
		this.color=color;
		this.type=type;
	}
}

let car1=new Car("Ford","Fiesta",2021,"red","sedan");
console.log(car1)